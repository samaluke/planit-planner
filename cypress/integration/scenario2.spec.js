// Scenario: Carl wants to make a goal to read 25 pages of the
//           book he's currently reading. To hold himself accountable,
//           he records the goal in the PlanIt Planner web app.

describe('Scenario 2 System Test', () => {
    it('Visits the PlanIt Planner Homepage', () => {
      cy.visit('http://127.0.0.1:8000/home/')
    })
  })

  describe('Scenario 2 System Test', () => {
    it('finds the content "New Goal"', () => {
      cy.visit('http://127.0.0.1:8000/home/')
  
      cy.contains('New Goal')
    })
  })

  describe('Scenario 2 System Test', () => {
    it('clicks the link "New Goal"', () => {
      cy.visit('http://127.0.0.1:8000/home/')
  
      cy.contains('New Goal').click()
    })
  })

  describe('Scenario 2 System Test', () => {
    it('clicking "New Goal" navigates to a new url', () => {
      cy.visit('http://127.0.0.1:8000/home/')
  
      cy.contains('New Goal').click()
  
      cy.url().should('include', '/goal/new')
    })
  })

  describe('Scenario 2 System Test', () => {
    it('Gets, types and asserts: New Event', () => {
      cy.visit('http://127.0.0.1:8000/home/')
  
      cy.contains('New Goal').click()
  
      cy.url().should('include', '/goal/new')

      cy.get('form').within(() => {
        cy.get('input#id_title').type('Read 25 pages')
        cy.get('textarea').type('Read 25 pages of any book currently reading.')
        cy.get('input#id_date').type('2020-11-25T17:30')
      })

      cy.contains('Submit').click()
    })
  })