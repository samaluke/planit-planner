// Scenario: Jenna is invited to a party for her coworker Martha's retirement
//           party at a local resturant, Jax Seafood. To make sure she doesn't
//           forget about the event, she records it into her calendar on
//           the PlanIt Planner web app.

  describe('Scenario 1 System Test', () => {
    it('Visits the PlanIt Planner Homepage', () => {
      cy.visit('http://127.0.0.1:8000/home/')
    })
  })

  describe('Scenario 1 System Test', () => {
    it('finds the content "Calendar"', () => {
      cy.visit('http://127.0.0.1:8000/home/')
  
      cy.contains('Calendar')
    })
  })

  describe('Scenario 1 System Test', () => {
    it('clicks the link "Calendar"', () => {
      cy.visit('http://127.0.0.1:8000/home/')
  
      cy.contains('Calendar').click()
    })
  })

  describe('Scenario 1 System Test', () => {
    it('clicking "Calendar" navigates to a new url', () => {
      cy.visit('http://127.0.0.1:8000/home/')
  
      cy.contains('Calendar').click()
  
      cy.url().should('include', '/calendar')
    })
  })

  describe('Scenario 1 System Test', () => {
    it('Gets, types and asserts: New Event', () => {
      cy.visit('http://127.0.0.1:8000/home/')
  
      cy.contains('Calendar').click()
  
      cy.url().should('include', '/calendar')
  
      cy.contains('New Event').click()

      cy.url().should('include', '/event/new')

      cy.get('form').within(() => {
        cy.get('input#id_title').type('Party at Jax')
        cy.get('textarea').type('Party for Marthas retirement at Jax Seafood.')
        cy.get('input#id_start_time').type('2020-11-19T17:30')
        cy.get('input#id_end_time').type('2020-11-19T19:30')
      })

      cy.contains('Submit').click()
    })
  })