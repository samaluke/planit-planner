from datetime import datetime, timedelta, date
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.conf import settings
import calendar
import random
import os
import sys
from .models import *
from .utils import Calendar
from .forms import EventForm, GoalForm, ToDoForm

cwd=os.getcwd()
files=os.listdir(cwd)
print("Files in %r: %s" % (cwd, files))

def index(request):
    return HttpResponse('hello')

def home(request):
    today = datetime.now().strftime("%I:%M%p on %B %d, %Y")
    with open(os.path.join(sys.path[0], "quotes.txt"), "r") as f:
        fdata=f.readlines()
        quote=random.choice(fdata)
    return render(request, 'cal/home.html', {'today': today, 'quote': quote})

class CalendarView(generic.ListView):
    model = Event
    template_name = 'cal/calendar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        d = get_date(self.request.GET.get('month', None))
        cal = Calendar(d.year, d.month)
        html_cal = cal.formatmonth(withyear=True)
        context['calendar'] = mark_safe(html_cal)
        context['prev_month'] = prev_month(d)
        context['next_month'] = next_month(d)
        return context

def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split('-'))
        return date(year, month, day=1)
    return datetime.today()

def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month

def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month

def event(request, event_id=None):
    instance = Event()
    if event_id:
        instance = get_object_or_404(Event, pk=event_id)
    else:
        instance = Event()
    
    form = EventForm(request.POST or None, instance=instance)
    if request.POST and form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('cal:calendar'))
    return render(request, 'cal/event.html', {'form': form})

def goal(request, goal_id=None):
    instance = Goal()
    if goal_id:
        instance = get_object_or_404(Goal, pk=goal_id)
    else:
        instance = Goal()
    
    form = GoalForm(request.POST or None, instance=instance)
    if request.POST and form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('cal:calendar'))
    return render(request, 'cal/goal.html', {'form': form})

def todo(request, todo_id=None):
    instace = ToDo()
    if todo_id:
        instance = get_object_or_404(ToDo, pk=todo_id)
    else:
        instance = ToDo()

    form=ToDoForm(request.POST or None, instance=instance)
    if request.POST and form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('cal:calendar'))
    return render(request, 'cal/todo.html', {'form': form})
