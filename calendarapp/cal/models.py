from django.db import models
from django.urls import reverse

class Event(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    @property
    def get_html_url(self):
        url = reverse('cal:event_edit', args=(self.id,))
        return f'<a href="{url}"> {self.title} </a>'

class Goal(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    date = models.DateTimeField()

    @property
    def get_html_url(self):
        url = reverse('cal:goal_edit', args=(self.id,))
        return f'<a href="{url}"> {self.title} </a>'

class ToDo(models.Model):
    title=models.CharField(max_length=200)
    date=models.DateTimeField()
    Completed=models.BooleanField(default=False)
    
    @property
    def get_html_url(self):
        url = reverse('cal:todo_edit', args=(self.id,))
        return f'<a href="{url}"> {self.title} </a>'
    
