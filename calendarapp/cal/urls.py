from django.conf.urls import url
from . import views

app_name = 'cal'
urlpatterns = [
    url(r'^index/$', views.index, name='index'),
    url(r'^home/$', views.home, name='home'),
    url(r'^calendar/$', views.CalendarView.as_view(), name='calendar'),
    url(r'^event/new/$', views.event, name='event_new'),
    url(r'^event/edit/(?P<event_id>\d+)/$', views.event, name='event_edit'),
    url(r'^goal/new/$', views.goal, name='goal_new'),
    url(r'^goal/edit/(?P<goal_id>\d+)/$', views.goal, name='goal_edit'),
    url(r'^todo/new/$', views.todo, name='todo_new'),
    url(r'^todo/edit/(?P<todo_id>\d+)/$', views.todo, name='todo_edit'),
]
