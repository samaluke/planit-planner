from django.test import TestCase, Client
from django.utils.timezone import now
from .models import Event, Goal, ToDo
from django.urls import reverse
from parameterized import parameterized
import datetime

class EventModelTest(TestCase):
    def test_event_title_char_field(self):
        title1 = Event.objects.create(title='This is the title', description='Descrition 1', start_time=now(), end_time=now())
        self.assertEqual('This is the title', title1.title)

    def test_event_description_field_value(self):
        text= Event.objects.create(title='title', description='Descrition 1', start_time=now(), end_time=now())
        self.assertEqual('Descrition 1', text.description)

    def test_event_start_datetime_field_value(self):
        starttime = Event.objects.create(title='title', description='Descrition 1', start_time=datetime.datetime(2020, 11, 11), end_time=now())
        day = datetime.datetime(2020, 11, 11)
        self.assertEqual(day, starttime.start_time)

    def test_event_end_datetime_field_value(self):
        endtime = Event.objects.create(title='title', description='Descrition 1', start_time=now(), end_time=now())
        future = now() + datetime.timedelta(days=20)
        self.assertNotEqual(future, endtime.end_time)

class GoalModelTest(TestCase):
    def test_goal_title_char_field(self):
        title1 = Goal.objects.create(title='This is the title', description='Descrition 1', date=now())
        self.assertEqual('This is the title', title1.title)

    def test_goal_description_field_value(self):
        text= Goal.objects.create(title='title', description='Descrition 1', date=now())
        self.assertEqual('Descrition 1', text.description)

    def test_goal_date_field_value(self):
        date1 = Goal.objects.create(title='title', description='Descrition 1', date=datetime.datetime(2020, 11, 11))
        day = datetime.datetime(2020, 11, 11)
        date2 = Goal.objects.create(title='title', description='Descrition 1', date=now())
        future = now() + datetime.timedelta(days=20)
        self.assertEqual(day, date1.date)
        self.assertNotEqual(future, date2)

    @parameterized.expand([
        ("title1", "Descirption 1", datetime.datetime(2020, 11, 11), datetime.datetime(2020, 11, 11)),
        ("title2", "Descirption 1", now(), now()),
    ])
    def test_goal_date_field_parameterized(self, inp1, inp2, inp3, expected):
        goal_obj = Goal.objects.create(title=inp1, description=inp2, date=inp3)
        self.assertEqual(goal_obj.date, expected)

class ToDoModelTest(TestCase):
    def test_todo_title_char_field(self):
        title1 = ToDo.objects.create(title='ToDo Test Title', date=now(), Completed=False)
        self.assertEqual('ToDo Test Title', title1.title)


    def test_todo_date_field_value(self):
        date1 = ToDo.objects.create(title='title', date=datetime.datetime(2020, 11, 20), Completed=False)
        day = datetime.datetime(2020, 11, 20)
        date2 = ToDo.objects.create(title='title', date=now(), Completed=False)
        future = now() + datetime.timedelta(days=20)
        self.assertEqual(day, date1.date)
        self.assertNotEqual(future, date2)

    def test_todo_completion_boolean(self):
        completed=ToDo.objects.create(title="completion test", date=now(), Completed=True)
        notCompleted=ToDo.objects.create(title="non-completion test", date=now(), Completed=False)
        self.assertTrue(completed.Completed)
        self.assertFalse(notCompleted.Completed)

    @parameterized.expand([
        ("title1", "Descirption 1", datetime.datetime(2020, 11, 11), datetime.datetime(2020, 11, 11)),
        ("title2", "Descirption 1", now(), now()),
    ])

    def test_goal_date_field_parameterized(self, inp1, inp2, inp3, expected):
        goal_obj = Goal.objects.create(title=inp1, description=inp2, date=inp3)
        self.assertEqual(goal_obj.date, expected)

class EventViewTest(TestCase):
    def test_event_view_no_event(self):
        respone = self.client.get(reverse('cal:calendar'))
        self.assertEqual(respone.status_code, 200)
        self.assertContains(respone, "")
        self.assertQuerysetEqual(respone.context['event_list'], [])

    @staticmethod
    def create_event(title, description, start_time, end_time):
        return Event.objects.create(title=title, description=description, start_time=start_time, end_time=end_time)

    @parameterized.expand([
        ("1st title", "description", now(), now(), ['<Event: Event object (1)>']),
        ("2nd title", "2nd description", datetime.datetime(2020, 11, 11), now() + datetime.timedelta(days=20), ['<Event: Event object (1)>']),
    ])
    def test_event_view(self, inp1, inp2, inp3, inp4, expected):
        EventViewTest.create_event(title=inp1, description=inp2, start_time=inp3, end_time=inp4)
        response = self.client.get(reverse('cal:calendar'))
        context = response.context['event_list']
        self.assertQuerysetEqual(context, expected)
