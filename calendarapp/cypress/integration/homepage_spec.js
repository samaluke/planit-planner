describe('Visit Plannit Planner', () => {
    it('Visits the homepage', () => {
        cy.visit('http://localhost:8000/home/')
    })
})

describe('Contains a type', () => {
    it('Finds the content "on"', () => {
        cy.visit('http://localhost:8000/home/')
  
        cy.contains('on')
    })
})